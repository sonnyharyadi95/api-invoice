<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\config\Helpers;

class ItemController extends Controller
{
    public function index(Request $request){
        $helper = new Helpers();
        $customer = Item::all();
        return response()->json(!$customer ? $helper->errorResponse() : $helper->successResponse($customer));
    }
}
