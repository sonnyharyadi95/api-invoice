<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Models\Item;
use App\Models\Supplier;
use App\Models\Customer;
use App\Models\InvoiceDetail;
use DB;
use Validator;
use App\config\Helpers;

class InvoiceController extends Controller
{
    private static $CREATE = "CREATE";
    private static $UPDATE = "UPDATE"; 

    public function generateObjProperties($obj, $arr){
        foreach ($arr as $key => $value) {
            $obj->$key = $value;
        }
        return $obj;
    }

    public function store(Request $request)
    {
        $helper = new Helpers();

        $validateInput = $this->validateInput($request, self::$CREATE, $helper);
        if (!empty($validateInput)) {
            return response()->json($helper->errorResponse($validateInput));
        }

        $detailItem = $request->item;
        $request->request->remove('item');

        DB::beginTransaction();
        try {
            $invoice = new Invoice($request->all());
            $invoice->save();
            
            /** SAVE INVOICE'S DETAIL */
            foreach ($detailItem as $key => $value) {
                $invoiceDetail = new InvoiceDetail();
                $invoiceDetail->invoice_id = $invoice->id;
                $this->generateObjProperties($invoiceDetail, $value);
                $invoiceDetail->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($helper->errorResponse($e->getMessage()));
        }
        return response()->json($helper->successResponse($invoice));
    }

    public function show($id)
    {
        $helper = new Helpers();
        $inv = Invoice::getDataInvoice($id);
        if (!$inv) {
            return response()->json($helper->errorResponse("Invoice " . $helper->DOESNT_EXIST));
        }
        $dtInv = InvoiceDetail::getDetailItem($inv->id);
        if (!$dtInv) {
            return response()->json($helper->errorResponse("Detail Invoice " . $helper->DOESNT_EXIST));
        }
        $total = 0;
        foreach ($dtInv as $key2 => $value2) {
            $total += $value2->subtotal;
        }
        
        $inv->item = $dtInv;
        $inv->total = number_format((float)$total, 2, '.', '');
        $inv->amount_due = number_format((float)(($total + $inv->tax_rate) - $inv->payment), 2, '.', '');
        return response()->json($helper->successResponse([$inv]));
    }

    public function showAllWithParam(Request $request){
        $helper = new Helpers();
        $query = [];

        foreach ($request->all() as $key => $value) {
            $query[] = [$key, '=', $value];
        }
        $inv = Invoice::where($query)->get();

        if (!$inv) {
            return response()->json($helper->errorResponse("Invoice " . $helper->DOESNT_EXIST));
        }
        
        foreach ($inv as $key => $value) {
            $amountDue = 0;
            $dtInv = InvoiceDetail::getDetailItem($value->id);
            $total = 0;
            foreach ($dtInv as $key2 => $value2) {
                $total += $value2->subtotal;
            }
            $inv[$key]['item'] = $dtInv;
            $inv[$key]['total'] = number_format((float)$total, 2, '.', '');
            $inv[$key]['amount_due'] = number_format((float)(($total + $inv[$key]->tax_rate) - $inv[$key]->payment), 2, '.', '');

        }
        return response()->json($helper->successResponse($inv));
    }

    public function update(Request $request, $id)
    {
        $helper = new Helpers();
        $validateInput = $this->validateInput($request, self::$UPDATE, $helper);
        if (!empty($validateInput)) {
            return response()->json($helper->errorResponse($validateInput));
        }

        $query = [];
        if (empty($request->all())) {
            return response()->json($helper->errorResponse($helper->NOTHING_TO_UPDATE));
        }

        $detailItem = $request->item;
        $request->request->remove('item');

        DB::beginTransaction();
        try {
            foreach ($request->all() as $key => $value) {
                $query[$key] = $value;
            }
            
            $inv = Invoice::where('id', $id)->update($query);
            if (!empty($detailItem)) {
                $querydt = [];
                foreach ($detailItem as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                        $querydt[$key2] = $value2;
                    }
                    if (empty($value['id'])) {
                        $invoiceDetail = new InvoiceDetail();
                        $invoiceDetail->invoice_id = $id;
                        $this->generateObjProperties($invoiceDetail, $value);
                        $invoiceDetail->save();
                    }
                    else{
                        $dtInv = InvoiceDetail::where([['invoice_id', $id], ['id', $value['id']]])->update($querydt);
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json($helper->errorResponse($e->getMessage()));
        }
        return response()->json($helper->successResponse($inv));
    }

    public function destroy($id)
    {
        $helper = new Helpers();
        $detailInv = InvoiceDetail::where('invoice_id', $id)->delete();
        if (!$detailInv) {
            return response()->json($helper->errorResponse("Detail Invoice " . $helper->DOESNT_EXIST));
        }
        $inv = Invoice::where('id', $id)->delete();
        if (!$inv) {
            return response()->json($helper->errorResponse("Invoice" . $helper->DOESNT_EXIST));
        }
        return response()->json($helper->successResponse($inv));
    }

    public function validateInput(Request $request, $source, $helper){
        
        $model = new Invoice();
        $validateInput = $source == self::$UPDATE ? $model->inputType : $model->inputTypeRequired;

        $validator = Validator::make($request->all(), $validateInput);
        if ($validator->fails()) {
            return $validator->errors()->first();
        }

        if (isset($request->supplier_id) && !Supplier::find($request->supplier_id)) {
            return "Supplier " . $helper->DOESNT_EXIST;
        }

        if (isset($request->customer_id) && !Customer::find($request->customer_id)) {
            return "Customer " . $helper->DOESNT_EXIST;
        }

        if (isset($request->item)) {
            foreach ($request->item as $key => $value) {
                $modelDetailItem = new InvoiceDetail();
                $validateInputItem = $source == self::$UPDATE ? $modelDetailItem->inputType : $modelDetailItem->inputTypeRequired;
                $validatorItem = Validator::make($request->item[$key], $validateInputItem);
                
                if ($validatorItem->fails()) {
                    return $validatorItem->errors()->first();
                }

                $itemCheck = Item::where('id', $value['item_id'])->first();
                if (!$itemCheck) {
                    return "Item " . $helper->DOESNT_EXIST;
                }
            }
        }
    }
}