<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\config\Helpers;

class CustomerController extends Controller
{
    public function index(Request $request){
        $helper = new Helpers();
        $customer = Customer::all();
        return response()->json(!$customer ? $helper->errorResponse() : $helper->successResponse($customer));
    }
}
