<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Invoice extends Model
{
    protected $table = 'tInvoice';

    protected $fillable = [
        'issue_date', 'due_date', 'subject', 
        'supplier_id', 'customer_id', 
        'tax_rate', 'status', 'payment'
    ];

    public $inputTypeRequired = [
        'issue_date' => 'required|date', 
        'due_date'=> 'required|date', 
        'subject'=> 'required|string', 
        'supplier_id'=> 'required|numeric', 
        'customer_id'=> 'required|numeric', 
        'tax_rate'=> 'required|numeric|min:0',
        'payment' => 'required|numeric|min:1'
    ];

    public $inputType = [
        'issue_date' => 'date', 
        'due_date'=> 'date', 
        'subject'=> 'string', 
        'supplier_id'=> 'numeric', 
        'customer_id'=> 'numeric', 
        'tax_rate'=> 'numeric|min:0',
        'payment' => 'required|numeric|min:1'
    ];

    public function getDataInvoice($id){
        $query = "SELECT INV.*, CUST.name cust_name, CUST.address cust_address, CUST.country cust_country, SUPPLIER.name sup_name, SUPPLIER.address sup_address, SUPPLIER.country sup_country
                  FROM tInvoice INV
                  INNER JOIN tCustomer CUST ON INV.customer_id = CUST.id
                  INNER JOIN tSupplier SUPPLIER ON INV.supplier_id = SUPPLIER.id
                  WHERE INV.ID = $id";
        return DB::select($query)[0];
    }
}
