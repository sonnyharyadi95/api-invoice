<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class InvoiceDetail extends Model
{
    protected $table = 'tInvoiceDetail';

    protected $fillable = [
        'invoice_id', 'item_id', 
        'qty', 'subtotal'
    ];

    public $inputTypeRequired = [
        'item_id'=> 'required|numeric', 
        'qty'=> 'required|numeric|min:1', 
        'subtotal'=> 'required|numeric|min:1'
    ];

    public $inputType = [
        'item_id'=> 'numeric', 
        'qty'=> 'numeric|min:1', 
        'subtotal'=> 'min:1'
    ];

    public function getDetailItem($id){
        $query = "SELECT INV_DT.*, ITEM.ID item_id, ITEM.type, ITEM.description, ITEM.unit_price
                  FROM tInvoiceDetail INV_DT
                  INNER JOIN tItem ITEM ON INV_DT.ITEM_ID = ITEM.ID
                  WHERE INV_DT.INVOICE_ID = $id
                  ";
        
        return DB::select($query);
    }
}
