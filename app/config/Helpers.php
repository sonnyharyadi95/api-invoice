<?php
namespace App\config;

class Helpers{

    private static $SUCCESS_MESSAGE = "Success load data";
    private static $ERROR_MESSAGE = "Failed load data";
    public $VALIDATION_ERROR = "Validation Error";
    public $DOESNT_EXIST = "Doesnt Exist";
    public $NOTHING_TO_UPDATE = "Nothing To Update";

    public static function successResponse($data = null, $message = null){
        $message = empty($message) ? self::$SUCCESS_MESSAGE : $message;
        return [
            "success" => true,
            "message" => $message,
            "data" => $data
        ];
    }

    public static function errorResponse($message = null){
        $message = empty($message) ? self::$ERROR_MESSAGE : $message;
        return [
            "success" => false,
            "message" => $message
        ];
    }
}
