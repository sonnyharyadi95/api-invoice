/*
 Navicat Premium Data Transfer

 Source Server         : localhost_wsl
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : 127.0.0.1:3306
 Source Schema         : db_init

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 20/01/2023 14:19:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tInvoiceDetail
-- ----------------------------
DROP TABLE IF EXISTS `tInvoiceDetail`;
CREATE TABLE `tInvoiceDetail`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(0) NULL DEFAULT NULL,
  `item_id` int(0) NULL DEFAULT NULL,
  `qty` int(0) NULL DEFAULT NULL,
  `subtotal` decimal(10, 2) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK Item`(`item_id`) USING BTREE,
  INDEX `FK Inv`(`invoice_id`) USING BTREE,
  CONSTRAINT `FK Item` FOREIGN KEY (`item_id`) REFERENCES `tItem` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK Inv` FOREIGN KEY (`invoice_id`) REFERENCES `tInvoice` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 48 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tInvoiceDetail
-- ----------------------------
INSERT INTO `tInvoiceDetail` VALUES (37, 24, 1, 15, 15.00, NULL, NULL);
INSERT INTO `tInvoiceDetail` VALUES (38, 24, 2, 10, 10.00, NULL, NULL);
INSERT INTO `tInvoiceDetail` VALUES (39, 24, 3, 1, 1.00, NULL, NULL);
INSERT INTO `tInvoiceDetail` VALUES (40, 28, 1, 15, 15.00, '2023-01-18 13:45:22', '2023-01-18 13:45:22');
INSERT INTO `tInvoiceDetail` VALUES (41, 28, 2, 10, 10.00, '2023-01-18 13:45:22', '2023-01-18 13:45:22');
INSERT INTO `tInvoiceDetail` VALUES (42, 28, 3, 1, 1.00, '2023-01-18 13:45:22', '2023-01-18 13:45:22');
INSERT INTO `tInvoiceDetail` VALUES (43, 29, 1, 15, 15.00, '2023-01-18 14:41:39', '2023-01-18 14:41:39');
INSERT INTO `tInvoiceDetail` VALUES (44, 29, 2, 10, 10.00, '2023-01-18 14:41:39', '2023-01-18 14:41:39');
INSERT INTO `tInvoiceDetail` VALUES (45, 29, 3, 1, 1.00, '2023-01-18 14:41:39', '2023-01-18 14:41:39');
INSERT INTO `tInvoiceDetail` VALUES (46, 30, 1, 5, 1000.00, '2023-01-19 09:58:54', '2023-01-20 06:02:33');
INSERT INTO `tInvoiceDetail` VALUES (47, 31, 3, 18, 3000.00, '2023-01-19 10:15:22', '2023-01-19 10:15:22');
INSERT INTO `tInvoiceDetail` VALUES (48, 34, 1, 15, 15.00, '2023-01-20 04:54:50', '2023-01-20 04:54:50');
INSERT INTO `tInvoiceDetail` VALUES (49, 34, 2, 10, 10.00, '2023-01-20 04:54:50', '2023-01-20 04:54:50');
INSERT INTO `tInvoiceDetail` VALUES (50, 34, 3, 1, 1.00, '2023-01-20 04:54:50', '2023-01-20 04:54:50');
INSERT INTO `tInvoiceDetail` VALUES (51, 36, 3, 35, 3000.00, '2023-01-20 04:58:10', '2023-01-20 05:27:36');
INSERT INTO `tInvoiceDetail` VALUES (52, 37, 1, 35, 1000.00, '2023-01-20 05:31:46', '2023-01-20 05:31:46');
INSERT INTO `tInvoiceDetail` VALUES (53, 30, 2, 10, 2000.00, '2023-01-20 05:38:17', '2023-01-20 06:02:33');
INSERT INTO `tInvoiceDetail` VALUES (54, 38, 2, 15, 2000.00, '2023-01-20 06:02:58', '2023-01-20 06:02:58');
INSERT INTO `tInvoiceDetail` VALUES (55, 39, 2, 15, 2000.00, '2023-01-20 06:06:59', '2023-01-20 06:40:17');

SET FOREIGN_KEY_CHECKS = 1;
