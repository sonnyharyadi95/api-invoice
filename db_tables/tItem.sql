/*
 Navicat Premium Data Transfer

 Source Server         : localhost_wsl
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : 127.0.0.1:3306
 Source Schema         : db_init

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 20/01/2023 14:19:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tItem
-- ----------------------------
DROP TABLE IF EXISTS `tItem`;
CREATE TABLE `tItem`  (
  `id` int(0) NOT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `unit_price` decimal(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tItem
-- ----------------------------
INSERT INTO `tItem` VALUES (1, 'Service', 'Design', 1000.00);
INSERT INTO `tItem` VALUES (2, 'Service', 'Development', 2000.00);
INSERT INTO `tItem` VALUES (3, 'Service', 'Meetings', 3000.00);

SET FOREIGN_KEY_CHECKS = 1;
