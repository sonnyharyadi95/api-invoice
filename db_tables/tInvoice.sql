/*
 Navicat Premium Data Transfer

 Source Server         : localhost_wsl
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : 127.0.0.1:3306
 Source Schema         : db_init

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 20/01/2023 14:18:55
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tInvoice
-- ----------------------------
DROP TABLE IF EXISTS `tInvoice`;
CREATE TABLE `tInvoice`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `issue_date` datetime(0) NULL DEFAULT NULL,
  `due_date` datetime(0) NULL DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `supplier_id` int(0) NULL DEFAULT NULL,
  `customer_id` int(0) NULL DEFAULT NULL,
  `tax_rate` int(0) NULL DEFAULT NULL,
  `payment` decimal(10, 2) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `invoice_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK Cust`(`customer_id`) USING BTREE,
  INDEX `FK Supplier`(`supplier_id`) USING BTREE,
  CONSTRAINT `FK Cust` FOREIGN KEY (`customer_id`) REFERENCES `tCustomer` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK Supplier` FOREIGN KEY (`supplier_id`) REFERENCES `tSupplier` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tInvoice
-- ----------------------------
INSERT INTO `tInvoice` VALUES (24, '2023-01-18 07:14:30', '2023-01-18 07:14:30', 'wow', 1, 1, 20, 120.00, NULL, NULL, NULL);
INSERT INTO `tInvoice` VALUES (26, '2023-01-18 07:14:30', '2023-01-18 07:14:30', 'wow', 1, 1, 20, NULL, '2023-01-18 13:02:11', '2023-01-18 13:02:11', NULL);
INSERT INTO `tInvoice` VALUES (28, '2023-01-18 07:14:30', '2023-01-18 07:14:30', 'wow', 1, 1, 20, 120.00, '2023-01-18 13:45:22', '2023-01-18 13:45:22', NULL);
INSERT INTO `tInvoice` VALUES (29, '2023-01-18 07:14:30', '2023-01-18 07:14:30', 'wow', 1, 1, 20, 120.00, '2023-01-18 14:41:39', '2023-01-18 14:41:39', NULL);
INSERT INTO `tInvoice` VALUES (30, '2022-12-29 00:00:00', '2023-01-28 00:00:00', 'random', 1, 1, 300, 250000.00, '2023-01-19 09:58:54', '2023-01-20 06:02:33', NULL);
INSERT INTO `tInvoice` VALUES (31, '2022-12-31 00:00:00', '2023-01-12 00:00:00', 'random', 1, 1, 1, 1.00, '2023-01-19 10:15:22', '2023-01-20 04:31:25', NULL);
INSERT INTO `tInvoice` VALUES (34, '2023-01-18 07:14:30', '2023-01-18 07:14:30', 'wow', 1, 1, 20, 120.00, '2023-01-20 04:54:50', '2023-01-20 04:54:50', NULL);
INSERT INTO `tInvoice` VALUES (36, '2022-12-31 00:00:00', '2023-01-30 00:00:00', 'random', 1, 1, 1, 1.00, '2023-01-20 04:58:10', '2023-01-20 05:27:36', NULL);
INSERT INTO `tInvoice` VALUES (37, '2023-01-04 00:00:00', '2023-01-24 00:00:00', 'random', 1, 2, 1, 1.00, '2023-01-20 05:31:46', '2023-01-20 05:31:46', NULL);
INSERT INTO `tInvoice` VALUES (38, '2023-01-01 00:00:00', '2023-01-14 00:00:00', 'random', 1, 2, 200, 350000.00, '2023-01-20 06:02:58', '2023-01-20 06:02:58', NULL);
INSERT INTO `tInvoice` VALUES (39, '2022-12-31 00:00:00', '2023-01-13 00:00:00', 'random', 1, 2, 200, 180000.00, '2023-01-20 06:06:59', '2023-01-20 06:40:17', NULL);

SET FOREIGN_KEY_CHECKS = 1;
