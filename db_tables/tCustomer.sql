/*
 Navicat Premium Data Transfer

 Source Server         : localhost_wsl
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : 127.0.0.1:3306
 Source Schema         : db_init

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 20/01/2023 14:18:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tCustomer
-- ----------------------------
DROP TABLE IF EXISTS `tCustomer`;
CREATE TABLE `tCustomer`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tCustomer
-- ----------------------------
INSERT INTO `tCustomer` VALUES (1, 'Barrington Publishers', '17 Great Suffolk Street London SE1 0NS', 'United Kingdom');
INSERT INTO `tCustomer` VALUES (2, 'Santika', '12 Greeeet', 'Spanyol');

SET FOREIGN_KEY_CHECKS = 1;
