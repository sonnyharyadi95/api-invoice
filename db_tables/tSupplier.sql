/*
 Navicat Premium Data Transfer

 Source Server         : localhost_wsl
 Source Server Type    : MySQL
 Source Server Version : 80031
 Source Host           : 127.0.0.1:3306
 Source Schema         : db_init

 Target Server Type    : MySQL
 Target Server Version : 80031
 File Encoding         : 65001

 Date: 20/01/2023 14:19:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tSupplier
-- ----------------------------
DROP TABLE IF EXISTS `tSupplier`;
CREATE TABLE `tSupplier`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `country` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tSupplier
-- ----------------------------
INSERT INTO `tSupplier` VALUES (1, 'Discovery Designs', '41 St Vincent Place Glasgow G1 2ER', 'Scotland');
INSERT INTO `tSupplier` VALUES (2, 'Aw', 'Aw', 'Aw');

SET FOREIGN_KEY_CHECKS = 1;
