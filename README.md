# Api Invoice
Nama: Sony Haryadi

deskripsi
1. komponen
    - lumen as PHP framework for Backend
    - html + native JS for Frontend
    - MySQL as database

2. installasi
    - composer install
    - pasang table (folder : db_tables). total table digunakan 5:
        - tCustomer.sql
        - tInvoice.sql
        - tInvoiceDetail.sql
        - tItem.sql
        - tSupplier.sql

3. jalankan program
    - untuk backend, jalankan program di localhost menggunakan port 8000 (http://localhost:8000)
    - untuk frontend, jalankan html
        - index.html untuk form Create, Update
        - invoice.html untuk Read, Delete

4. Notes
    - untuk invoice diasumsikan pembayaran hanya 1x untuk alasan simplisitas, sehingga nominal pembayaran disimpan di tabel header invoice
    - untuk supplier, item, customer. diasumsikan data diambil dari table.