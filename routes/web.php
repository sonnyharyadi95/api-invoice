<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('/', 'InvoiceController@store');
$router->post('/update_invoice/{id}', 'InvoiceController@update');
$router->delete('/{id}', 'InvoiceController@destroy');
$router->get('/invoice_list', 'InvoiceController@showAllWithParam');
$router->get('/invoice_list/{id}', 'InvoiceController@show');
$router->get('/customer', 'CustomerController@index');
$router->get('/item', 'ItemController@index');